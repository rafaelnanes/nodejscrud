var mysql = require('mysql');

module.exports.conectar = function (pQuery, pParams, pCbGetResultado) {
    if (typeof pParams == undefined) {
        pParams = [];
    }

    var pool = mysql.createPool({
        connectionLimit: 100, //important
        host: 'localhost',
        user: 'root',
        password: '1234',
        database: 'nodejscrud',
        debug: false
    });

    var conexao = function (pError, pConexao) {
        
        if (pError) {
            throw (pError);
        } else {
            console.log('Conexão criada');
        }

        //console.log('Conectado com id ' + pConexao.threadId);

        pConexao.query(pQuery, pParams, function (err, resultado) {
            if (!err) {
                pConexao.release();
                pCbGetResultado(resultado);
            } else {
                throw (err);
            }
        });

       
    };
    pool.getConnection(conexao);

}



