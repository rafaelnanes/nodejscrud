module.exports = function (pApp) {
    var usuario = require('./../models/usuario');
    var IndexController = {
        index: function (pReq, pRes) {
            usuario.buscarTodos(function (pObjResponse) {
                pRes.render('index', pObjResponse.getRetorno());
            });
        }
    };
    return IndexController;
};

