var express = require('express');
var load = require('express-load');
var app = express();
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var cookieSession = require('cookie-session');
var methodOverride = require('method-override');
var error = require('./middleware/erros');

app.set('views', __dirname + '/views');
app.set('view engine', 'jade');

//-=-=-=-=-=-=-=-= ENGINES -=-=-=-=-=-=-=-=-=-=-=-=
app.use(cookieParser());
app.use(cookieSession({ //sessão
	name: 'usuario',
	keys: ['nome', 'email', 'contatos'],
	resave: true,
	saveUninitialized: false,
}));
app.use(bodyParser.json());//converte a requisição de formulario em objeto
app.use(bodyParser.urlencoded({ extended: true }));// eh o type do formulário 
app.use(methodOverride('X-HTTP-Method-Override'));//permite utilizar um mesmo path entre os métodos do HTTP,fazendo uma sobrescrita de métodos
app.use(express.static(__dirname + '/public'));//diz para o express que os recursos estáticos estarão nessa página

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
load('models')
	.then('controllers')
	.then('routes')
	.into(app);


module.exports = app;
//-=-=-=-=-=-=-=-= CONTROLANDO REQUISIÇÕES -=-=-=-=-=-=-=-=-=-=-=-=
app.use(error.notFound);
//app.use(error.serverError);